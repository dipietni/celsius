package sheridan;
//Nicholas Di Pietrantonio
import static org.junit.Assert.*;
import sheridan.Main;

import org.junit.Test;

public class MainTest {

	@Test
	public void testFromFahrenheitRegular() {
		assertTrue(Main.fromFahrenheit(32) == 0);
	}
	@Test
	public void testFromFahrenheitException() {
		assertFalse(Main.fromFahrenheit(100) == 0);
	}
	@Test
	public void testFromFahrenheitIn() {
		assertTrue(Main.fromFahrenheit(31) == 0);
	}
	@Test
	public void testFromFahrenheitOut() {
		assertFalse(Main.fromFahrenheit(34) == 0);
	}

}
