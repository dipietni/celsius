/* Nicholas Di Pietrantonio - 991444656*/
package test;

import static org.junit.Assert.*;
import password.*;

import org.junit.Test;

public class PasswordTest {
	
	@Test
	public void testValidateUpperCase() throws Exception {
		boolean val = Password.validateUpperCase("AAAAaaaa");
		assertTrue(val == true);
	}
	
	@Test (expected=Exception.class)
	public void testValidateUpperCaseException() throws Exception {
		boolean val = Password.validateUpperCase("asdasdasd");
		fail();
	}
	
	@Test
	public void testValidateUpperCaseIn() throws Exception {
		boolean val = Password.validateUpperCase("Nicholas");
		assertTrue(val == true);
	}
	
	@Test (expected=Exception.class)
	public void testValidateUpperCaseOut() throws Exception {
		boolean val = Password.validateUpperCase("NICHOLAS");
		assertFalse(val == false);
	}
	
	
	

	@Test
	public void testValidatePasswordLengthRegular() throws Exception {
		boolean val = Password.validatePasswordLength("Nicholasss");
		assertTrue(val == true);
	}
	
	@Test (expected=Exception.class)
	public void testValidatePasswordLengthException() throws Exception {
		boolean val = Password.validatePasswordLength("Nich");
		fail();
	}
	
	@Test
	public void testValidatePasswordLengthIn() throws Exception {
		boolean val = Password.validatePasswordLength("Nicholas");
		assertTrue(val == true);
	}
	
	@Test (expected=Exception.class)
	public void testValidatePasswordLengthOut() throws Exception {
		boolean val = Password.validatePasswordLength("Nichola");
		assertFalse(val == false);
	}
	
	@Test
	public void testValidateNumbersRegular() throws Exception {
		boolean val = Password.validatePasswordNumbers("Nicholas123");
		assertTrue(val == true);
	}
	
	@Test (expected=Exception.class)
	public void testValidateNumbersRegularException() throws Exception {
		boolean val = Password.validatePasswordNumbers("Nicholas");
		fail();
	}
	
	@Test
	public void testValidateNumbersIn() throws Exception {
		boolean val = Password.validatePasswordNumbers("Nicholas12");
		assertTrue(val == true);
	}
	
	@Test (expected=Exception.class)
	public void testValidateNumbersOut() throws Exception {
		boolean val = Password.validatePasswordNumbers("Nicholas1");
		assertFalse(val == true);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
